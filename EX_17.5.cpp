
#include <iostream>

class MyVeryFirstClass
{
private:
	int A = 32;

public:
	void ShowA()
	{
		std::cout << "\n" << "1st ex: Your hidden number is " << A;
	}
};

class Vector
{
private:
	int x;
	int y;
	int z;
public:
	
	Vector() : x(4), y(4), z(4)
	{}

	void ShowVector()
	{
		std::cout << "\n\n" << "2nd ex: Length of the vector is " << sqrt(x * x + y * y + z * z) << "\n";
	}
	
};

int main()
{
	MyVeryFirstClass temp;
	temp.ShowA();

	Vector v;
	v.ShowVector();
}

